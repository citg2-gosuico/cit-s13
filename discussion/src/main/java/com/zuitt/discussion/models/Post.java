package com.zuitt.discussion.models;


import javax.persistence.*;

// Marks this java object as a representation of an entity/record from the database table
@Entity
// Designate the table name related to the model.
@Table(name = "posts")
public class Post {

// Properties
// Indicated the primary key
        @Id
// Id auto incremented
        @GeneratedValue
        private Long id;

// Class properties that represents table column in a relational database are annotated as @Column

        @Column
        private String title;
        @Column
        private String content;

        // establishes the relationship of the property to the "post" model
        @ManyToOne
        // sets the relationship to this property and user_id column in the databse to the primary key of the user model.
        @JoinColumn(name="user_id", nullable = false)
        private User user;

// Constructor
// Default constructors are required when retrieving data from the database.
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

// Getters and Setters

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getUser(){
        return user;
    }
    public void setUser(User user){
        this.user = user;
    }
}
